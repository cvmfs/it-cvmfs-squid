#!/usr/bin/perl -w

use Socket ;
#use Socket6 qw( getaddrinfo getnameinfo inet_pton pack_sockaddr_in6 );
my $log = '/var/log/squid/access.log' ;

my $last = 60 * 60 * 24 ;
my $total = 0 ;
my $start = time - $last ;
my %hosts ;
my %domains ;
open (LOG,$log) ;
while (<LOG>) {
  chomp ;

  if ( m/^(\d+)\.\d.*$/)  {
     if ($1 > $start ) {
        if (/^\S+\s+\S+\s+(\S+)\s.*/) {
           $total++ ;
           my $ip = $1 ;
           if ( $hosts{$ip} ) {
             $hosts{$ip}{'count'}++ ;
             my $d = $hosts{$ip}{'domain'} ;
             $domains{$d}{'count'}++ ;
           } else {
             $hosts{$ip}{'count'}  = 1 ;
             my $abc = &host2ip($ip) ;
             $hosts{$ip}{'host'} = $abc ;
             $abc =~ s/^[^\.]*//;
             $hosts{$ip}{'domain'} = $abc ;
             if ( $domains{$abc}{'count'} ) {
                $domains{$abc}{'count'}++;
             } else {
                $domains{$abc}{'count'} = 1;
             }
           }
        }
     }
  }
}
close(LOG) ;
my $date = `date`;
chomp($date);
print "\nNumber of file requests per domain and host generated at ".$date."\nfor the last $last seconds\n\n";

foreach my $d ( sort { $domains{$b}{'count'} <=> $domains{$a}{'count'} } keys %domains ) {
   my $percent = sprintf "%.2f", ($domains{$d}{'count'} / $total)  * 100 ;
   print $domains{$d}{'count'}.' ('.$percent.'%) '.$d."\n" ;
   foreach my $i ( sort { $hosts{$b}{'count'} <=> $hosts{$a}{'count'} } keys %hosts ) {
         if ( $hosts{$i}{'domain'} eq $d ) {
             my $percent = sprintf "%.2f", ($hosts{$i}{'count'} / $total)  * 100 ;
             print "\t".$hosts{$i}{'count'}.' ('.$percent.'%) '.$hosts{$i}{'host'}." $i\n" ;
         }
   }
}


sub host2ip (){
   my $ip_addr = shift ;
   my $host ;
   my $port ;
   if (length($ip_addr) >= 20) {
     my $ip6_packed = inet_pton(AF_INET6, $ip_addr);
     my $sockaddr = pack_sockaddr_in6(80, $ip6_packed);
     ($host,$port) = getnameinfo( $sockaddr );
   } else {
     $host = gethostbyaddr( inet_aton( $ip_addr ), AF_INET );
   }
   return $ip_addr unless $host;
   return $host;
}






