sources:
rpm:
	rpmbuild -ba --define 'dist .ai7' --define "_sourcedir $(PWD)" *.spec

scratch:
	koji build ai7 --nowait --scratch  git+http://git.cern.ch/cernpub/it-cvmfs-squid.git#$(shell git rev-parse HEAD)

build:
	koji build ai7 --nowait git+http://git.cern.ch/cernpub/it-cvmfs-squid.git#$(shell git rev-parse HEAD)


