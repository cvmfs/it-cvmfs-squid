Name:           it-cvmfs-squid
Version:        0.8
Release:        1%{?dist}
Summary:        Random scripts to generate reports for cvmfs.

Group:          Mine
License:        WTF
URL:            https://svnweb.cern.ch/trac/batchinter/browser
Source0:        CVMFS-stratum-reporting.conf
Source1:        CVMFS-stratum-reporting.cron
Source2:        requests-24hours.pl
Source3:        README
Source4:        volume-24hours.pl

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:        perl(Socket6)

%description
Some random scripts for parsing cvmfs squid files to get useful stuff.

%prep
cp -p %{SOURCE0} %{SOURCE1} %{SOURCE2} %{SOURCE3} %{SOURCE4} .

%build
# nothing.

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d
mkdir -p %{buildroot}%{_sysconfdir}/cron.d
mkdir -p %{buildroot}%{_var}/lib/CVMFS-stratum-reporting
install -p -m 755 requests-24hours.pl %{buildroot}%{_sbindir}/requests-24hours.pl
install -p -m 755 volume-24hours.pl %{buildroot}%{_sbindir}/volume-24hours.pl
install -p -m 644 CVMFS-stratum-reporting.conf %{buildroot}%{_sysconfdir}/httpd/conf.d/CVMFS-stratum-reporting.conf
install -p -m 644 CVMFS-stratum-reporting.cron %{buildroot}%{_sysconfdir}/cron.d/CVMFS-stratum-reporting.cron
install -p -m 644 README %{buildroot}%{_var}/lib/CVMFS-stratum-reporting/README

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/httpd/conf.d/CVMFS-stratum-reporting.conf
%config(noreplace) %{_sysconfdir}/cron.d/CVMFS-stratum-reporting.cron
%{_var}/lib/CVMFS-stratum-reporting/README
%{_sbindir}/requests-24hours.pl
%{_sbindir}/volume-24hours.pl
%changelog
* Wed Jan 21 2014 Steve Traylen <steve.traylen@cern.ch> - 0.6-1
- Add IPv6 support badly, it's perl.

* Thu Feb 9 2012 Steve Traylen <steve.traylen@cern.ch> - 0.4-1
- Go to two decimal places.

* Wed Feb 7 2012 Steve Traylen <steve.traylen@cern.ch> - 0.3-1
- Add volume script..

* Wed Feb 7 2012 Steve Traylen <steve.traylen@cern.ch> - 0.3-1
- Add volume script..

* Wed Feb 7 2012 Steve Traylen <steve.traylen@cern.ch> - 0.2-1
- Add percentages.

* Wed Feb 7 2012 Steve Traylen <steve.traylen@cern.ch> - 0.1-1
- Initial Version.


